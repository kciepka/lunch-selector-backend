const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const config = require("./config.json");
const authRoutes = require("./routes/auth");
const userRoutes = require("./routes/user");
const votesRoutes = require("./routes/votes");
const matchesRoutes = require("./routes/matches");
const dbClient = require("./db/client");

module.exports = {
  async init() {
    const app = express();

    app.locals.db = await dbClient.getMongoConnection();

    app.use(cors());
    app.use(bodyParser.json());
    app.use("/auth", authRoutes);
    app.use("/matches", matchesRoutes);
    app.use("/user", userRoutes);
    app.use("/votes", votesRoutes);

    app.listen(config.port, () =>
      console.log(`App is running on port ${config.port}`)
    );
  }
};
