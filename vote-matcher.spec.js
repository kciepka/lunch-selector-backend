const voteMatcher = require("./vote-matcher");
const Vote = require("./vote.model");

let vote1 = new Vote().create("useridtest",
    {
        places: ["place1", "place2"],
        hours: [{ from: new Date(), to: new Date() }]
    });

let vote2 = new Vote().create("otheruser",
    {
        places: ["place3", "place4"],
        hours: [{ from: new Date(), to: new Date() }]
    });

let vote3 = new Vote().create("another",
    {
        places: ["place5", "place6"],
        hours: [{ from: new Date(), to: new Date() }]
    });

let vote4 = new Vote().create("fourth",
    {
        places: ["place5", "place3"],
        hours: [{ from: new Date(), to: new Date() }]
    });

console.log(voteMatcher.matchAllVotes([vote1, vote2, vote3, vote4]));