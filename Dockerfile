FROM arm32v7/node
WORKDIR /var/www/html
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 80
CMD ["npm", "start"]