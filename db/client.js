const mongo = require("mongodb");
const config = require("../config.json");

module.exports = {
  getMongoConnection() {
    const client = new mongo.MongoClient(config.dbUrl, {
      useNewUrlParser: true
    });

    return new Promise((resolve, reject) => {
      client.connect((err, dbClient) => {
        if (err) return reject(err);
        const db = dbClient.db(config.dbName);
        resolve(db);
      });
    });
  }
};
