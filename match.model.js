class Match {
  constructor() {}

  create(user1, user2, commonPlaces, commonDateRange) {
    this.commonPlaces = commonPlaces;
    this.commonDateRange = commonDateRange;
    this.users = [user1, user2];

    return this;
  }
}

module.exports = Match;
