const { google } = require("googleapis");
const GoogleAuthClient = require("./google-oauth-client");

module.exports = (req, res, next) => {
  let googleAuth = new GoogleAuthClient(google);

  if (
    !req.headers.authorization ||
    req.headers.authorization.split(" ")[0] !== "Bearer"
  ) {
    res.status(401).end();
  }

  googleAuth
    .verifyIdToken(req.headers.authorization.split(" ")[1])
    .then(data => {
      const payload = data.getPayload();
      req.user = {
        id: payload["sub"],
        givenName: payload["given_name"],
        familyName: payload["family_name"],
        picture: payload["picture"]
      };
      next();
    })
    .catch(err => {
      console.error(err);
      res.status(401).end();
    });
};
