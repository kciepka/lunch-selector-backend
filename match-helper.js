const _ = require("lodash");

module.exports = {
  getCommonDateRange(dateRange1, dateRange2) {
    let d1s = dateRange1[0].getTime();
    let d1e = dateRange1[1].getTime();
    let d2s = dateRange2[0].getTime();
    let d2e = dateRange2[1].getTime();

    let commonRangeStartDate, commonRangeEndDate;

    if (d1s > d1e || d2s > d2e) throw new Error("Invalid date range");

    if (d1s < d2s) {
      if (d1e < d2s) return null;
      commonRangeStartDate = new Date(d2s);
      commonRangeEndDate = d2e > d1e ? new Date(d1e) : new Date(d2e);
    } else {
      if (d2e < d1s) return null;
      commonRangeStartDate = new Date(d1s);
      commonRangeEndDate = d1e > d2e ? new Date(d2e) : new Date(d1e);
    }
    return [commonRangeStartDate, commonRangeEndDate];
  },

  getCommonPlaces(places) {
    return _.intersection(...places);
  },

  getMostPopularPlace(commonPlaces) {
    let allPlaces = _.flatten(commonPlaces);
    let mostPopular = _.head(
      _(allPlaces)
        .countBy()
        .entries()
        .maxBy("[1]")
    );

    return mostPopular;
  }
};
