class Vote {
  constructor() { }

  create(user, params) {
    this.timestamp = new Date();
    this.user = user;
    this.places = params.places;
    this.hours = params.hours;

    return this;
  }
}

module.exports = Vote;
