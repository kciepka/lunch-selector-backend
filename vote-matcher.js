const _ = require("lodash");
const matchHelper = require("./match-helper");
const Match = require("./match.model");

module.exports = {
  matchVotes(vote1, vote2) {
    let commonDateRange = matchHelper.getCommonDateRange(
      [new Date(vote1.hours[0].from), new Date(vote1.hours[0].to)],
      [new Date(vote2.hours[0].from), new Date(vote2.hours[0].to)]
    );

    let commonPlaces = matchHelper.getCommonPlaces([
      vote1.places,
      vote2.places
    ]);

    return new Match().create(
      vote1.user,
      vote2.user,
      commonPlaces,
      commonDateRange
    );
  },

  matchAllVotes(votes) {
    let matches = [];

    if (votes) {
      votes.forEach(vote1 => {
        let votesWithoutCurrent = _.without(votes, vote1);
        votesWithoutCurrent.forEach(vote2 => {
          let newMatch = this.matchVotes(vote1, vote2);
          let isUnique = matches.findIndex(match => {
            return ((match.users[1].id == newMatch.users[0].id) && (match.users[0].id == newMatch.users[1].id))
          }) == -1;

          if (isUnique) {
            matches.push(newMatch);
          }
        });
      });
    }

    return matches;
  }
};
