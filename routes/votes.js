const express = require("express");
const router = express.Router();
const VotesDao = require("../dao/votes.dao");
const { celebrate, Joi, errors } = require("celebrate");
const tokenInterceptor = require("../token-interceptor.middleware");
const Vote = require("../vote.model");

router.use(tokenInterceptor);

router.get("/", (req, res) => {
  const votesDao = new VotesDao(req.app.locals.db);

  votesDao
    .getVotes()
    .then(result => {
      res.json(result);
    })
    .catch(err => {
      console.error(err);
      res.status(500).end();
    });
});

router.get("/today", (req, res) => {
  const votesDao = new VotesDao(req.app.locals.db);

  votesDao
    .getTodayVotes()
    .then(result => {
      res.json(result);
    })
    .catch(err => {
      console.error(err);
      res.status(500).end();
    });
});

router.post(
  "/",
  celebrate({
    body: Joi.object().keys({
      places: Joi.array()
        .items(Joi.string().required())
        .required(),
      hours: Joi.array()
        .items(
          Joi.object({
            from: Joi.date()
              .iso()
              .required(),
            to: Joi.date()
              .iso()
              .required()
          }).required()
        )
        .required()
    })
  }),
  (req, res) => {
    const votesDao = new VotesDao(req.app.locals.db);
    let vote = new Vote().create(req.user, req.body);

    votesDao
      .addVote(vote)
      .then(() => {
        res.status(201).end();
      })
      .catch(err => {
        console.error(err);
        res.status(500).end();
      });
  }
);

router.use(errors());

module.exports = router;
