const express = require("express");
const { google } = require("googleapis");
const router = express.Router();
const GoogleAuthClient = require("../google-oauth-client");

router.get("/", (req, res) => {
  let googleAuth = new GoogleAuthClient(google);

  let url = googleAuth.getAuthUrl();

  res.json({ url: url });
});

router.get("/tokens", (req, res) => {
  let googleAuth = new GoogleAuthClient(google);

  let code = req.query.code;

  googleAuth
    .getClient()
    .getToken(code)
    .then(data => {
      res.json(data.tokens);
    })
    .catch(err => {
      console.error(err);
      return res.status(503).end();
    });
});

router.post("/refreshTokens", (req, res) => {
  let googleAuth = new GoogleAuthClient(google);

  let refreshToken = req.body.refresh_token;

  googleAuth
    .getClient()
    .refreshToken(refreshToken)
    .then(data => {
      res.json(data.tokens);
    })
    .catch(err => {
      console.error(err);
      return res.status(503).end();
    });
});

module.exports = router;
