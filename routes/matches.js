const express = require("express");
const voteMatcher = require("../vote-matcher");
const VotesDao = require("../dao/votes.dao");
const tokenInterceptor = require("../token-interceptor.middleware");
const router = express.Router();

router.use(tokenInterceptor);

router.get("/", (req, res) => {
  new VotesDao(req.app.locals.db).getVotes().then(votes => {
    try {
      let matches = voteMatcher.matchAllVotes(votes);
      res.json(matches);
    }
    catch (ex) {
      console.error(ex);
      res.status(500).end();
    }
  });
});

router.get("/today", (req, res) => {
  new VotesDao(req.app.locals.db).getTodayVotes().then(votes => {
    try {
      let matches = voteMatcher.matchAllVotes(votes);
      res.json(matches);
    }
    catch (ex) {
      console.error(ex);
      res.status(500).end();
    }
  });
});

module.exports = router;
