const express = require("express");
const router = express.Router();
const tokenInterceptor = require("../token-interceptor.middleware");

router.use(tokenInterceptor);

router.get("/", (req, res) => {
  res.json(req.user);
});

module.exports = router;
