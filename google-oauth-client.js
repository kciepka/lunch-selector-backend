const config = require("./config.json");

class GoogleOAuthClient {
  constructor(google) {
    var _client = new google.auth.OAuth2(
      config.googleAuthClientID,
      config.googleAuthSecret,
      config.googleAuthRedirectURI
    );

    this.getClient = () => {
      return _client;
    };
  }

  getAuthUrl() {
    const url = this.getClient().generateAuthUrl({
      access_type: "offline",
      scope: config.googleAuthScopes
    });

    return url;
  }

  verifyIdToken(token) {
    return this.getClient().verifyIdToken({
      idToken: token,
      audience: config.googleAuthClientID
    });
  }
}

module.exports = GoogleOAuthClient;
