const assert = require("assert");

class VotesDao {
  constructor(db) {
    this.db = db;
  }

  async getVotes() {
    return await this.db
      .collection("votes")
      .find({}, { projection: { _id: 0 } })
      .toArray();
  }

  async getTodayVotes() {
    return await this.db
      .collection("votes")
      .find(
        {
          timestamp: {
            $lt: new Date(),
            $gte: new Date(new Date().setHours(0, 0, 0, 0))
          }
        },
        { projection: { _id: 0 } }
      )
      .toArray();
  }

  async addVote(vote) {
    let result = await this.db.collection("votes").insertOne(vote);
    assert.equal(1, result.insertedCount);
  }
}

module.exports = VotesDao;
