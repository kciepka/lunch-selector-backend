let matchHelper = require("./match-helper");

console.log(
  matchHelper.getMostPopularPlace([
    ["test3", "test2", "test1"],
    ["test2", "test1"],
    ["test1", "test4"]
  ])
);

console.log(
  matchHelper.getCommonPlaces([
    ["test1", "test2", "test4"],
    ["test3", "test2", "test1"]
  ])
);

let dateRange1 = [
  new Date("2018-10-10T13:00:00Z"),
  new Date("2018-10-10T13:30:00Z")
];

let dateRange2 = [
  new Date("2018-10-10T13:15:00Z"),
  new Date("2018-10-10T13:45:00Z")
];

console.log(matchHelper.getCommonDateRange(dateRange1, dateRange2));

let dateRange3 = [
  new Date("2018-10-10T13:00:00Z"),
  new Date("2018-10-10T13:30:00Z")
];

let dateRange4 = [
  new Date("2018-10-10T13:31:00Z"),
  new Date("2018-10-10T13:50:00Z")
];

console.log(matchHelper.getCommonDateRange(dateRange3, dateRange4));
